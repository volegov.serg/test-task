package com.rincentral.test;

import com.rincentral.test.eventhandling.DataStorage;
import com.rincentral.test.models.BodyCharacteristics;
import com.rincentral.test.models.EngineCharacteristics;
import com.rincentral.test.models.external.ExternalCarFullInfo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.rincentral.test.models.external.enums.EngineType.*;
import static com.rincentral.test.models.external.enums.FuelType.GASOLINE;
import static com.rincentral.test.models.external.enums.GearboxType.*;
import static com.rincentral.test.models.external.enums.WheelDriveType.*;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest
class TestApplicationTests {

    @MockBean
    private DataStorage dataStorage;

    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    public void init() {
        List<ExternalCarFullInfo> testExternalCarFullInfoList = new ArrayList<>();
        testExternalCarFullInfoList.add(new ExternalCarFullInfo(9,
            "B-segment",
            "Volkswagen",
            "Polo",
            "Germany",
            "Mk6",
            "1.4 TSI",
            "2020-present",
            FWD,
            ROBOTIC,
            9.2,
            204,
            new EngineCharacteristics(GASOLINE, L4, 1395, 125),
            new BodyCharacteristics(4469, 1706, 1471, "Hatchback")));

        testExternalCarFullInfoList.add(new ExternalCarFullInfo(6,
            "B-segment",
            "Lada",
            "Granta",
            "Russia",
            "I generation",
            "Sport",
            "2011-present",
            FWD,
            MANUAL,
            9.5,
            197,
            new EngineCharacteristics(GASOLINE, L4, 1597, 120),
            new BodyCharacteristics(4260, 1700, 1500, "Sedan,  Hatchback,  Wagon")));

        testExternalCarFullInfoList.add(new ExternalCarFullInfo(86,
            "F-segment",
            "Mercedes",
            "S-Class",
            "Germany",
            "W140",
            "250 L",
            "1991-1998",
            RWD,
            AUTO,
            6.6,
            250,
            new EngineCharacteristics(GASOLINE, V12, 5987, 394),
            new BodyCharacteristics(5215, 1886, 1485, "Sedan")));

        testExternalCarFullInfoList.add(new ExternalCarFullInfo(
            91,
            "F-segment",
            "Jaguar",
            "XJ",
            "England",
            "V generation",
            "5.0 S",
            "2009-present",
            AWD,
            AUTO,
            4.9,
            250,
            new EngineCharacteristics(GASOLINE, V8, 5000, 5100),
            new BodyCharacteristics(5255, 1899, 1460, "Sedan")));

        Set<String> testFuelTypeSet = new HashSet<>();
        testFuelTypeSet.add(GASOLINE.toString());

        Set<String> testBodyStyleSet = new HashSet<>();
        testBodyStyleSet.add("Sedan");
        testBodyStyleSet.add("Hatchback");
        testBodyStyleSet.add("Wagon");

        Set<String> testEngineTypeSet = new HashSet<>();
        testEngineTypeSet.add(V8.toString());
        testEngineTypeSet.add(V12.toString());
        testEngineTypeSet.add(L4.toString());

        Set<String> testWheelDriveTypeSet = new HashSet<>();
        testWheelDriveTypeSet.add(AWD.toString());
        testWheelDriveTypeSet.add(RWD.toString());
        testWheelDriveTypeSet.add(FWD.toString());

        Set<String> testGearboxesSet = new HashSet<>();
        testGearboxesSet.add(ROBOTIC.toString());
        testGearboxesSet.add(MANUAL.toString());
        testGearboxesSet.add(AUTO.toString());

        when(dataStorage.getExternalCarFullInfoList()).thenReturn(testExternalCarFullInfoList);
        when(dataStorage.getFuelTypeSet()).thenReturn(testFuelTypeSet);
        when(dataStorage.getBodyStyleSet()).thenReturn(testBodyStyleSet);
        when(dataStorage.getEngineTypeSet()).thenReturn(testEngineTypeSet);
        when(dataStorage.getWheelDriveTypeSet()).thenReturn(testWheelDriveTypeSet);
        when(dataStorage.getGearboxTypeSet()).thenReturn(testGearboxesSet);
    }

    @Test
    public void testCars() throws Exception {
        this.mockMvc.perform(get("/api/cars"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(4)));
    }

    @Test
    public void testCarsCountry() throws Exception {
        this.mockMvc.perform(get("/api/cars?country=England"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(1)))
            .andExpect(jsonPath("$.[0]", hasKey("country")))
            .andExpect(jsonPath("$.[0].country", is("England")));
    }

    @Test
    public void testCarsSegment() throws Exception {
        this.mockMvc.perform(get("/api/cars?segment=F-segment"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(2)))
            .andExpect(jsonPath("$.[0]", hasKey("segment")))
            .andExpect(jsonPath("$.[0].segment", is("F-segment")));
    }

    @Test
    public void testCarsMinEngineDisplacement() throws Exception {
        this.mockMvc.perform(get("/api/cars?minEngineDisplacement=4000"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(2)));
    }

    @Test
    public void testCarsMinEngineHorsepower() throws Exception {
        this.mockMvc.perform(get("/api/cars?minEngineHorsepower=500"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(1)));
    }

    @Test
    public void testCarsMinMaxSpeed() throws Exception {
        this.mockMvc.perform(get("/api/cars?minMaxSpeed=250"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(2)));
    }

    @Test
    public void testCarsSearch() throws Exception {
        String searchString = "ant";

        this.mockMvc.perform(get("/api/cars?search=" + searchString))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(1)))
            .andExpect(jsonPath("$.[0]", hasKey("model")))
            .andExpect(jsonPath("$.[0].model", containsString(searchString)));
    }

    @Test
    public void testCarsIsFull() throws Exception {
        this.mockMvc.perform(get("/api/cars?isFull=true"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(4)))
            .andExpect(jsonPath("$.[0]", hasKey("body")))
            .andExpect(jsonPath("$.[0].body", hasKey("body_length")))
            .andExpect(jsonPath("$.[0].body", hasKey("body_height")))
            .andExpect(jsonPath("$.[0].body", hasKey("body_width")))
            .andExpect(jsonPath("$.[0].body", hasKey("body_style")))
            .andExpect(jsonPath("$.[0]", hasKey("engine")))
            .andExpect(jsonPath("$.[0].engine", hasKey("engine_type")))
            .andExpect(jsonPath("$.[0].engine", hasKey("engine_cylinders")))
            .andExpect(jsonPath("$.[0].engine", hasKey("engine_displacement")))
            .andExpect(jsonPath("$.[0].engine", hasKey("engine_horsepower")));
    }

    @Test
    public void testCarsYear() throws Exception {
        this.mockMvc.perform(get("/api/cars?year=2010"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(1)));
    }

    @Test
    public void testCarsBodyStyle() throws Exception {
        this.mockMvc.perform(get("/api/cars?bodyStyle=Sedan"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(3)));
    }

    @Test
    public void testFuelTypes() throws Exception {
        this.mockMvc.perform(get("/api/fuel-types"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasItem(GASOLINE.toString())));
    }

    @Test
    public void testBodyStyles() throws Exception {
        this.mockMvc.perform(get("/api/body-styles"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasItem("Sedan")));
    }

    @Test
    public void testEngineTypes() throws Exception {
        this.mockMvc.perform(get("/api/engine-types"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasItem(L4.toString())));
    }

    @Test
    public void testWheelDrives() throws Exception {
        this.mockMvc.perform(get("/api/wheel-drives"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasItem(AWD.toString())));
    }

    @Test
    public void testGearboxes() throws Exception {
        this.mockMvc.perform(get("/api/gearboxes"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasItem(ROBOTIC.toString())));
    }

    @Test
    public void testMaxSpeedWithMissingModel() throws Exception {
        String model = "Tuareg";

        this.mockMvc.perform(get("/api/max-speed?model=" + model))
            .andExpect(status().isNotFound());
    }

    @Test
    public void testMaxSpeedWithModelAndBrand() throws Exception {
        this.mockMvc.perform(get("/api/max-speed?model=XJ&brand=Mercedes"))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void testMaxSpeedWithBrand() throws Exception {
        this.mockMvc.perform(get("/api/max-speed?brand=Mercedes"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", is(250.0)));
    }

    @Test
    public void testMaxSpeedWithModelQuery() throws Exception {
        this.mockMvc.perform(get("/api/max-speed?model=XJ"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", is(250.0)));
    }
}
