package com.rincentral.test.eventhandling;

import com.rincentral.test.models.BodyCharacteristics;
import com.rincentral.test.models.EngineCharacteristics;
import com.rincentral.test.models.external.ExternalBrand;
import com.rincentral.test.models.external.ExternalCar;
import com.rincentral.test.models.external.ExternalCarFullInfo;
import com.rincentral.test.models.external.ExternalCarInfo;
import com.rincentral.test.services.ExternalCarsApiService;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class DataStorage {
    private final ExternalCarsApiService externalCarsApiService;
    @Getter
    private final Set<String> fuelTypeSet = new HashSet<>();
    @Getter
    private final Set<String> engineTypeSet = new HashSet<>();
    @Getter
    private final Set<String> wheelDriveTypeSet = new HashSet<>();
    @Getter
    private final Set<String> gearboxTypeSet = new HashSet<>();
    @Getter
    private List<ExternalCarFullInfo> externalCarFullInfoList = new ArrayList<>();
    @Getter
    private Set<String> bodyStyleSet = new HashSet<>();

    @EventListener
    public void handleContextRefreshEvent(ContextRefreshedEvent ctxStartEvt) {
        List<ExternalCar> externalCarList = externalCarsApiService.loadAllCars();
        Map<Integer, ExternalBrand> brandMap = externalCarsApiService.loadAllBrands().stream().collect(Collectors.toMap(ExternalBrand::getId, b -> b));
        List<ExternalCarInfo> externalCarInfoList = new ArrayList<>();
        ExternalCarInfo externalCarInfo;
        List<List<String>> bodyStyleList = new ArrayList<>();
        for (ExternalCar externalCar : externalCarList) {
            externalCarInfo = externalCarsApiService.loadCarInformationById(externalCar.getId());
            externalCarInfoList.add(externalCarInfo);
            fuelTypeSet.add(externalCarInfo.getFuelType().toString());
            bodyStyleList.add(Arrays.stream(externalCarInfo.getBodyStyle().split(",  ")).collect(Collectors.toList()));
            engineTypeSet.add(externalCarInfo.getEngineType().toString());
            wheelDriveTypeSet.add(externalCarInfo.getWheelDriveType().toString());
            gearboxTypeSet.add(externalCarInfo.getGearboxType().toString());
        }
        bodyStyleSet = bodyStyleList.stream().flatMap(List::stream).collect(Collectors.toSet());

        externalCarFullInfoList = externalCarInfoList
            .stream()
            .map(c -> new ExternalCarFullInfo(c.getId(),
                c.getSegment(),
                brandMap.get(c.getBrandId()).getTitle(),
                c.getModel(),
                brandMap.get(c.getBrandId()).getCountry(),
                c.getGeneration(),
                c.getModification(),
                c.getYearsRange(),
                c.getWheelDriveType(),
                c.getGearboxType(),
                c.getAcceleration(),
                c.getMaxSpeed(),
                new EngineCharacteristics(c.getFuelType(), c.getEngineType(), c.getEngineDisplacement(), c.getHp()),
                new BodyCharacteristics(c.getBodyLength(), c.getBodyWidth(), c.getBodyHeight(), c.getBodyStyle())))
            .collect(Collectors.toList());
    }
}
