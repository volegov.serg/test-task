package com.rincentral.test.controllers;

import com.rincentral.test.eventhandling.DataStorage;
import com.rincentral.test.models.CarFullInfo;
import com.rincentral.test.models.CarInfo;
import com.rincentral.test.models.external.ExternalCarFullInfo;
import com.rincentral.test.models.params.CarRequestParameters;
import com.rincentral.test.models.params.MaxSpeedRequestParameters;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class ApiController {

    private final DataStorage dataStorage;

    @GetMapping("/cars")
    public ResponseEntity<List<? extends CarInfo>> getCars(CarRequestParameters requestParameters) {
        List<ExternalCarFullInfo> externalCarFullInfoList = dataStorage.getExternalCarFullInfoList();

        if (requestParameters.getCountry() != null)
            externalCarFullInfoList = externalCarFullInfoList.stream().filter(c -> c.getCountry().equals(requestParameters.getCountry())).collect(Collectors.toList());

        if (requestParameters.getSegment() != null)
            externalCarFullInfoList = externalCarFullInfoList.stream().filter(c -> c.getSegment().equals(requestParameters.getSegment())).collect(Collectors.toList());

        if (requestParameters.getMinEngineDisplacement() != null)
            externalCarFullInfoList = externalCarFullInfoList.stream().filter(c -> c.getEngineCharacteristics().getEngineDisplacement() >= requestParameters.getMinEngineDisplacement()).collect(Collectors.toList());

        if (requestParameters.getMinEngineHorsepower() != null)
            externalCarFullInfoList = externalCarFullInfoList.stream().filter(c -> c.getEngineCharacteristics().getEngineHorsePower() >= requestParameters.getMinEngineHorsepower()).collect(Collectors.toList());

        if (requestParameters.getMinMaxSpeed() != null)
            externalCarFullInfoList = externalCarFullInfoList.stream().filter(c -> c.getMaxSpeed() >= requestParameters.getMinMaxSpeed()).collect(Collectors.toList());

        if (requestParameters.getSearch() != null)
            externalCarFullInfoList = externalCarFullInfoList.stream().filter(c -> c.getGeneration().contains(requestParameters.getSearch()) || c.getModel().contains(requestParameters.getSearch()) || c.getModification().contains(requestParameters.getSearch())).collect(Collectors.toList());

        String s = "";
        List<ExternalCarFullInfo> yearExternalCarFullInfoList = new ArrayList<>();
        if (requestParameters.getYear() != null) {
            for (ExternalCarFullInfo externalCarFullInfo : externalCarFullInfoList) {
                s = externalCarFullInfo.getYearsRange();
                if (s.contains("present")) {
                    int year = Calendar.getInstance().get(Calendar.YEAR);
                    s = s.replace("present", Integer.toString(year));
                }
                List<Integer> rangeList = Arrays.stream(s.split("-")).collect(Collectors.toList()).stream().map(Integer::parseInt).collect(Collectors.toList());

                if (rangeList.get(0) <= requestParameters.getYear() && requestParameters.getYear() <= rangeList.get(1)) {
                    yearExternalCarFullInfoList.add(externalCarFullInfo);
                }
            }
            externalCarFullInfoList = yearExternalCarFullInfoList;
        }

        if (requestParameters.getBodyStyle() != null)
            externalCarFullInfoList = externalCarFullInfoList.stream().filter(c -> Arrays.stream(c.getBodyCharacteristics().getBodyStyle().split(",  ")).anyMatch(bs -> bs.equals(requestParameters.getBodyStyle()))).collect(Collectors.toList());

        List<? extends CarInfo> carInfoList;
        if (requestParameters.getIsFull() != null && requestParameters.getIsFull())
            carInfoList = externalCarFullInfoList.stream().map(c -> new CarFullInfo(c.getId(), c.getSegment(), c.getBrand(), c.getModel(), c.getCountry(), c.getGeneration(), c.getModification(), c.getEngineCharacteristics(), c.getBodyCharacteristics())).collect(Collectors.toList());
        else {
            carInfoList = externalCarFullInfoList.stream().map(c -> new CarInfo(c.getId(), c.getSegment(), c.getBrand(), c.getModel(), c.getCountry(), c.getGeneration(), c.getModification())).collect(Collectors.toList());
        }
        if (carInfoList.isEmpty())
            return ResponseEntity.notFound().build();

        return ResponseEntity.ok(carInfoList);
    }

    @GetMapping("/fuel-types")
    public ResponseEntity<List<String>> getFuelTypes() {
        return ResponseEntity.ok(new ArrayList<>(dataStorage.getFuelTypeSet()));
    }

    @GetMapping("/body-styles")
    public ResponseEntity<List<String>> getBodyStyles() {
        return ResponseEntity.ok(new ArrayList<>(dataStorage.getBodyStyleSet()));
    }

    @GetMapping("/engine-types")
    public ResponseEntity<List<String>> getEngineTypes() {
        return ResponseEntity.ok(new ArrayList<>(dataStorage.getEngineTypeSet()));
    }

    @GetMapping("/wheel-drives")
    public ResponseEntity<List<String>> getWheelDrives() {
        return ResponseEntity.ok(new ArrayList<>(dataStorage.getWheelDriveTypeSet()));
    }

    @GetMapping("/gearboxes")
    public ResponseEntity<List<String>> getGearboxTypes() {
        return ResponseEntity.ok(new ArrayList<>(dataStorage.getGearboxTypeSet()));
    }

    @GetMapping("/max-speed")
    public ResponseEntity<Double> getMaxSpeed(MaxSpeedRequestParameters requestParameters) {
        List<ExternalCarFullInfo> externalCarFullInfoList = dataStorage.getExternalCarFullInfoList();
        if (requestParameters.getBrand() != null && requestParameters.getModel() != null)
            return ResponseEntity.badRequest().build();

        if (requestParameters.getBrand() != null)
            externalCarFullInfoList = externalCarFullInfoList.stream().filter(c -> c.getBrand().equals(requestParameters.getBrand())).collect(Collectors.toList());

        if (requestParameters.getModel() != null)
            externalCarFullInfoList = externalCarFullInfoList.stream().filter(c -> c.getModel().equals(requestParameters.getModel())).collect(Collectors.toList());

        if (externalCarFullInfoList.isEmpty())
            return ResponseEntity.notFound().build();

        OptionalDouble averageMaxSpeed = externalCarFullInfoList.stream().mapToDouble(ExternalCarFullInfo::getMaxSpeed).average();
        return ResponseEntity.ok(averageMaxSpeed.isPresent() ? averageMaxSpeed.getAsDouble() : 0.0);
    }
}
