package com.rincentral.test.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class CarFullInfo extends CarInfo{

    @JsonProperty("engine")
    private EngineCharacteristics engineCharacteristics;

    @JsonProperty("body")
    private BodyCharacteristics bodyCharacteristics;

    public CarFullInfo(Integer id,
                       String segment,
                       String brand,
                       String model,
                       String country,
                       String generation,
                       String modification,
                       EngineCharacteristics engineCharacteristics,
                       BodyCharacteristics bodyCharacteristics) {
        super(id, segment, brand, model, country, generation, modification);

        this.engineCharacteristics = engineCharacteristics;
        this.bodyCharacteristics = bodyCharacteristics;
    }
}
