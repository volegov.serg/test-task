package com.rincentral.test.models.external;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.rincentral.test.models.BodyCharacteristics;
import com.rincentral.test.models.CarInfo;
import com.rincentral.test.models.EngineCharacteristics;
import com.rincentral.test.models.external.enums.GearboxType;
import com.rincentral.test.models.external.enums.WheelDriveType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ExternalCarFullInfo extends CarInfo {

    private String yearsRange;

    private WheelDriveType wheelDriveType;

    private GearboxType gearboxType;

    private Double acceleration;

    private Integer maxSpeed;

    @JsonProperty("engine")
    private EngineCharacteristics engineCharacteristics;

    @JsonProperty("body")
    private BodyCharacteristics bodyCharacteristics;

    public ExternalCarFullInfo(
        Integer id,
        String segment,
        String brand,
        String model,
        String country,
        String generation,
        String modification,
        String yearsRange,
        WheelDriveType wheelDriveType,
        GearboxType gearboxType,
        Double acceleration,
        Integer maxSpeed,
        EngineCharacteristics engineCharacteristics,
        BodyCharacteristics bodyCharacteristics
    ) {
        super(id, segment, brand, model, country, generation, modification);

        this.yearsRange = yearsRange;
        this.wheelDriveType = wheelDriveType;
        this.gearboxType = gearboxType;
        this.acceleration = acceleration;
        this.maxSpeed = maxSpeed;
        this.engineCharacteristics = engineCharacteristics;
        this.bodyCharacteristics = bodyCharacteristics;
    }
}
